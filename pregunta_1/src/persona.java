public class persona {

	//Atributos
    private String nombre;
    private tarjeta cuenta;
    private data data;
   
   //Constructor
    persona(){
    	cuenta = new tarjeta();
    	data = new data();
    }
    
    //Metodos
    public void setname(String nombre){
    	this.nombre = nombre;
    }
    
    public String getname(){
    	return this.nombre;
    }
    
    public tarjeta getcuenta(){
    	return cuenta;
    }
    
    public data getdata(){
    	return data;
    }
	
}
