import java.util.Scanner;
public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		persona persona;
		int resp;
		String respuesta;
		Scanner sc = new Scanner(System.in);		

		persona = new persona();
		
		persona.setname(persona.getdata().setname());
		persona.getcuenta().setsaldo(persona.getdata().setsaldo());
		
		respuesta = "";
		
		while(respuesta.equals("")){
		    System.out.println("¿Que accion desea realizar?\nPresione 1 para mostrar informacion actual." +
		                       "\nPresione 2 para realizar una compra" +
			    	           "\nPresione 3 para realizar un abono");
		
		    resp = persona.getdata().resp();
		
		    if (resp == 1){
		        System.out.println("Usuario: " + persona.getname());
		        System.out.print("Saldo: " + "$" + persona.getcuenta().getsaldo() + "\n");
		    }
		
		    else if (resp == 2){
		        persona.getdata().calculo(persona.getcuenta().getsaldo(),persona.getdata().setdif());
		        persona.getcuenta().setsaldo(persona.getdata().getdif());
		        System.out.println("Usuario: " + persona.getname());
		        System.out.println("Nuevo saldo: $" + persona.getdata().getdif());
		    }
		
		    else if(resp == 3){
			    persona.getdata().calculo(persona.getcuenta().getsaldo(),-persona.getdata().setdif());
		        persona.getcuenta().setsaldo(persona.getdata().getdif());
		        System.out.println("Usuario: " + persona.getname());
		        System.out.println("Nuevo saldo: $" + persona.getdata().getdif() + "\n");
		    }
		
		    else {
			    System.out.println("Asegurese de ingresar la opcion 1, 2 o 3...");
		    }
		    
		    System.out.println("Presione enter si desea continuar\n" +
	                   "En caso de no querer continuar presione cualquier tecla\n");
		    
		    respuesta = sc.nextLine();
	    }
	}
}
