import java.util.Scanner;
public class data {
	
	//Atributos
	private String nombre;
	private Double saldo;
	private int resp;
	private double dif;
	private String respuesta;
	
	Scanner sc = new Scanner(System.in);
	
    //Constructor
	data(){}
	
	//Metodos
	public String setname(){
		System.out.println("Ingrese su nombre: ");
		nombre = sc.nextLine();
		return nombre;
	}
	
	public double setsaldo(){
		System.out.println("Ingrese su saldo: ");
		saldo = sc.nextDouble();
		return saldo;
	}
	
	public double setdif(){
		System.out.println("Ingrese el total de su compra: ");
		dif = sc.nextDouble();
		return dif;
	}
	public double calculo(double param_saldo, double param_dif){
		this.saldo = param_saldo - param_dif;
		return this.saldo;
	}
	
	public double getdif(){
		return this.saldo;
	}
	
    public int resp(){
		resp = sc.nextInt();
		return resp;
	}
}
