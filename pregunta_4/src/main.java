import java.util.Scanner;
public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		persona persona;
		int opcion;
		int transferencia;
		int abono;
		String resp;
		cuenta_destino cuenta;
		Scanner sc = new Scanner(System.in);
		
		persona = new persona();
		cuenta = new cuenta_destino(10000,"2020430009");
		persona.setnombre();
		persona.gettarjeta().setsaldo();
		resp = "";
		
		while(resp.equals("")){
			System.out.println("Presione 1 para realizar una transferencia\n" +
			                   "Presione 2 para recibir un abono\n" + 
					           "Presione 3 para ver su cuenta\n\n");
			
			opcion = sc.nextInt();
			
			if(opcion == 1){
				System.out.println("Ingrese el monto a transferir a la cuenta " + cuenta.getnumero());
				transferencia = sc.nextInt();
				
				if(transferencia > persona.gettarjeta().getsaldo()){
					System.out.println("Error\n");
				}
				
				else if(transferencia <= persona.gettarjeta().getsaldo()){
					persona.gettarjeta().actualizasaldo(persona.gettarjeta().getsaldo() - transferencia);
					System.out.println("Transferencia realizada exitosamente!\n");
				}
				
				else {}
			}
			
			else if(opcion == 2){
				System.out.println("Ingrese el monto de el abono\n");
				abono = sc.nextInt();
				
				if(abono<0){
					System.out.println("Error\n");
				}
				
				else if(abono>=0){
					persona.gettarjeta().actualizasaldo(persona.gettarjeta().getsaldo() + abono);
					System.out.println("Transferencia realizada exitosamente!\n");
				}
				
				else {}
			}
			
			else if(opcion == 3){
				persona.getdatos();
			}
			else {
				System.out.println("Error");
			}
			System.out.println("Presione enter si desea continuar\n" +
			                   "En caso de no querer continuar presione cualquier tecla\n");
			
			//Se utilizan dos resp ya que asi logre que funcionara el ciclo, con solo un resp 
			//se continua con el ciclo indefinidamente
			resp = sc.nextLine();
			resp = sc.nextLine();
		}
	}

}
