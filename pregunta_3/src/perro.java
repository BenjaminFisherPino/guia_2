import java.util.Scanner;
public class perro {

	//Atributos de la clase
	private String nombre;
	private String color;
	private String raza;
    private String sexo;
    private int peso;
    private int edad;
    
    private perro amigo;
    private String ladrar;
    private String jugar;
    private String comer;
    private String territorial;
    private String estado;
    
    Scanner sc = new Scanner(System.in);

    //Constructores
    
    //Constructor 1) sirve para crear mascotas ingresadas por el usuario
    perro(){
    	System.out.println("Ingrese el nombre de su mascota: ");
    	this.nombre = sc.nextLine();
    	System.out.println("Ingrese el color de su mascota: ");
    	this.color = sc.nextLine();
    	System.out.println("Ingrese la raza de su mascota: ");
    	this.raza = sc.nextLine();
    	System.out.println("Ingrese el sexo de su mascota: ");
    	this.sexo = sc.nextLine();
    	System.out.println("Ingrese la edad de su mascota: ");
    	this.edad = sc.nextInt();
    	System.out.println("Ingrese el peso de su mascota: ");
    	this.peso = sc.nextInt();
    }
    
    //Constructor 2) sirve para crear las mascotas desde el codigo
    perro(String param_nombre, String param_color, String param_raza, 
          int param_edad, String param_sexo, int param_peso){
    	this.nombre = param_nombre;
    	this.color = param_color;
    	this.raza = param_raza;
    	this.edad = param_edad;
    	this.sexo = param_sexo;
    	this.peso = param_peso;
    }
    
    //Metodos
    public void muestrainfo(){
    	System.out.println("\nNombre: " + this.nombre +
    			           "\nColor: " + this.color +
    			           "\nRaza: " + this.raza +
    			           "\nEdad: " + this.edad + " años" +
    			           "\nSexo: " + this.sexo +
    			           "\nPeso: " + this.peso + " Kilos");
    }
    
    public void muestrainfo2() {
    	System.out.println("¿Esta ladrando su perro?\n");
    	this.ladrar = sc.nextLine();
    	System.out.println("¿Esta jugando su perro?\n");
    	this.ladrar = sc.nextLine();
    	System.out.println("¿Esta comiendo su perro?\n");
    	this.comer = sc.nextLine();
    	System.out.println("¿Es territorial su perro?");
    	this.territorial = sc.nextLine();
    	System.out.println("¿Cual es el estado de su perro?");
    	this.estado = sc.nextLine();
    }
    
    public String getnombre(){
    	return this.nombre;
    }
    
    public void setladrar(String param_ladrar){
    	this.ladrar = param_ladrar;	
    }
    
    public String getladrar(){
    	return this.ladrar;
    }

    public void setjugar(String param_jugar, perro param_perro){
    	this.jugar = param_jugar;
    	this.amigo = param_perro;
    }
    
    public String getjugar(){
    	return this.jugar;
    }

    public void setcomer(String param_comer){
    	this.comer = param_comer;
    }

    public String getcomer(){
    	return this.comer;
    }
    
    public void setterritorial(String param_territorial){
    	this.territorial = param_territorial;
    }
    
    public String getterritorial(){
    	return this.territorial;
    }
    
    public void setestado(String param_estado){
    	this.estado = param_estado;
    }
    
    public String getestado(){
    	return this.estado;
    }
    
    public void muestrametodo(){
    	System.out.println("\n" + this.ladrar +
    			           "\n" + this.jugar + 
    			           "\n" + this.comer +
    			           "\n" + this.territorial +
    			           "\n" + this.estado + "\n");
    }
}